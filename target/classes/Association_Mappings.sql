--Many - To -one

ALTER TABLE Employee
ADD address    INT NOT NULL;

create table ADDRESS (
   id INT NOT NULL auto_increment,
   street_name VARCHAR(40) default NULL,
   city_name VARCHAR(40) default NULL,
   state_name VARCHAR(40) default NULL,
   zipcode VARCHAR(10) default NULL,
   PRIMARY KEY (id)
);


INSERT INTO ADDRESS (ID, street_name, city_name, state_name, zipcode)
VALUES (2, 'Martires', 'Mexico', 'CDMX', '14269');

UPDATE EMPLOYEE SET ADDRESS=2 WHERE ID IN (19,20,21);

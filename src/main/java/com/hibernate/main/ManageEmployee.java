package com.hibernate.main;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.hibernate.model.Address;
import com.hibernate.model.Certificate;
import com.hibernate.model.Employee;

public class ManageEmployee {
	private static SessionFactory factory; 
	   public static void main(String[] args) {
	      
	      try {
	         factory = new Configuration().configure().buildSessionFactory();
	      } catch (Throwable ex) { 
	         System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex); 
	      }
	      
	      ManageEmployee ME = new ManageEmployee();

	      /* Let us have one address object */
	      Address address = ME.addAddress("Kondapur","Hyderabad","AP","532");

	      /* Let us have a set of certificates for the first employee  */
	      HashSet set1 = new HashSet();
	      set1.add(new Certificate("MCA"));
	      set1.add(new Certificate("MBA"));
	      set1.add(new Certificate("PMP"));
	      
	      /* Add employee records in the database */
	      Integer empID1 = ME.addEmployee("Manoj", "Kumar", address, set1);
	      
	      /* Another set of certificates for the second employee  */
	      HashSet set2 = new HashSet();
	      set2.add(new Certificate("BCA"));
	      set2.add(new Certificate("BA"));

	      /* Add another employee record in the database */
	      Integer empID2 = ME.addEmployee("Dilip", "Kumar", address,set2);

	      /* List down all the employees */
	      ME.listEmployees();

	      /* Update employee's role records */
	      ME.updateEmployee(empID1, "HR");

	      /* Delete an employee from the database */
	      ME.deleteEmployee(empID2);

	      /* List down all the employees */
	      ME.listEmployees();

	   }

	   /* Method to add an address record in the database */
	   public Address addAddress(String street, String city, String state, String zipcode) {
	      Session session = factory.openSession();
	      Transaction tx = null;
	      Integer addressID = null;
	      Address address = null;
	      
	      try {
	         tx = session.beginTransaction();
	         address = new Address(street, city, state, zipcode);
	         addressID = (Integer) session.save(address); 
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	      return address;
	   }

	   /* Method to add an employee record in the database */
	   public Integer addEmployee(String fname, String role,Address address,Set cert){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      Integer employeeID = null;
	      
	      try {
	         tx = session.beginTransaction();
	         Employee employee = new Employee(fname, role, new Date(), address, cert);
	         employeeID = (Integer) session.save(employee); 
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	      return employeeID;
	   }

	   /* Method to list all the employees detail */
	   public void listEmployees( ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      
	      try {
	         tx = session.beginTransaction();
	         List employees = session.createQuery("FROM Employee").list(); 
	         for (Iterator iterator = employees.iterator(); iterator.hasNext();){
	            Employee employee = (Employee) iterator.next(); 
	            System.out.print("Name: " + employee.getName()); 
	            System.out.print("  ID: " + employee.getId()); 
	            System.out.println("  Role: " + employee.getRole());
	            Address add = employee.getAddress();
	            System.out.println("Address ");
	            System.out.println("\tStreet: " +  add.getStreet());
	            System.out.println("\tCity: " + add.getCity());
	            System.out.println("\tState: " + add.getState());
	            System.out.println("\tZipcode: " + add.getZipcode());
	            Set certificates = employee.getCertificates();
	             for (Iterator iterator2 = certificates.iterator(); iterator2.hasNext();){
	                Certificate certName = (Certificate) iterator2.next(); 
	                System.out.println("Certificate: " + certName.getName()); 
	             }
	         }
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	   }
	   
	   /* Method to update salary for an employee */
	   public void updateEmployee(Integer EmployeeID, String role ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      
	      try {
	         tx = session.beginTransaction();
	         Employee employee = (Employee)session.get(Employee.class, EmployeeID); 
	         employee.setRole( role );
	         session.update(employee);
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	   }
	   
	   /* Method to delete an employee from the records */
	   public void deleteEmployee(Integer EmployeeID){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      
	      try {
	         tx = session.beginTransaction();
	         Employee employee = (Employee)session.get(Employee.class, EmployeeID); 
	         session.delete(employee); 
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	   }
}

package com.hibernate.main;

import java.util.Date;
import java.util.HashSet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.hibernate.model.anotation.Address;
import com.hibernate.model.anotation.Certificate;
import com.hibernate.model.anotation.Employee;
import com.hibernate.util.HibernateUtil;

public class HibernateAnnotationMain {

	public static void main(String[] args) {

		/* Let us have one address object */
		Address address = new Address("Kondapur", "Hyderabad", "AP", "532");

	

		Employee emp = new Employee();
		emp.setName("David");
		emp.setRole("Developer");
		emp.setInsertTime(new Date());

		// Get Session
		SessionFactory sessionFactory = HibernateUtil.getSessionAnnotationFactory();
		Session session = sessionFactory.getCurrentSession();
		// start transaction
		session.beginTransaction();
		Certificate c1 = new Certificate("MCA"); 
		Certificate c2 = new Certificate("MBA"); 
		Certificate c3 = new Certificate("PMP"); 
		
		
		// Save the Model object
		session.save(address);
		emp.setAddress(address);
		
		/* Let us have a set of certificates for the first employee */
		HashSet set1 = new HashSet();
		
		
		set1.add(c1);
		set1.add(c2);
		set1.add(c3);
		emp.setCertificates(set1);
		
		session.save(emp);
		// Commit transaction
		session.getTransaction().commit();
		System.out.println("Employee ID=" + emp.getId());

		// terminate session factory, otherwise program won't end
		sessionFactory.close();
	}

}

package com.hibernate.model;

import java.util.Date;
import java.util.Set;

public class Employee {

	private int id;
	private String name;
	private String role;
	private Date insertTime;
	private Address address;
	private Set certificates;

	public Employee() {

	}
	
	public Employee(String name, String role, Date insertTime, Address address, Set certificates) {
		super();
		this.name = name;
		this.role = role;
		this.insertTime = insertTime;
		this.certificates = certificates;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set getCertificates() {
		return certificates;
	}

	public void setCertificates(Set certificates) {
		this.certificates = certificates;
	}

}
